# Clarifications

## 1. Do we need to store the list of accessible modules per role in the database?

If so, I imagine the table(s) would look like:
#### Modules Table
|Field|Type |Purpose|
|-----|-----|-------|
|Id|int|Primary key|
|Module|nvarchar(250)|This field will identify the module accessible by this role. E.g. admin-bypass, accounts, premium-settings, etc.|

### Role Modules Table
|Field|Type |Purpose|
|-----|-----|-------|
|Id|int|Primary key|
|RoleId|int|Foreign key to ListEnum role|
|ModuleId|int|Foreign key to Modules table|

### Additional front-end changes

With this we can retrieve the role of the current user from the front-end and retrieve all of the accessible modules.

We can also create a helper file that would contain a function called *canAccess* which accepts a string parameter.

In case of the navigation bar in the super admin app, we can say:
```javascript
<div *ngIf="canAccess('admin-bypass')">Admin Bypass</div>
```

The can access table will rely on the list of modules currently accessible by the user/role.

We'll likely have a route `/api/modules`, the HTTP Client or the browser would provide the Authorization header.

The back-end code will determine the user's role and return the list of accessible modules. This will be done during start-up of the web application, similar to how our styling settings are being retrieved.
```json
[
    {
        id: 1,
        module: 'admin-bypass'
    },
    {
        id: 1,
        module: 'accounts'
    }
]
```

## 2. (Security) Do we need to validate ownership of the database record?

Assuming I am logged in an admin account under Webcape. I can theoretically update(or delete) other accounts using the same token. Given that we're already using *UNIQUEIDENTIFIER* and not int's in our primary keys this will be a little bit harder to do, but the vulnerability is there.

*to be continued (this is where things get tricky)*